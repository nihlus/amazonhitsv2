
const KEY = {
    RECENT: 'recentconfigs'
};


function addToRecent(filepath, name) {

    let recent = getRecent();
    let found = recent.find(item => item.filepath === filepath);
    if (found) return;

    recent.push({
        filepath,
        name
    });

    recent = recent.slice(-5);
    set(KEY.RECENT, JSON.stringify(recent));
}


function getRecent() {
    return JSON.parse(get(KEY.RECENT, '[]'));
}


function set(key, value) {
  window.localStorage[key] = value;
}


function get(key, defaultValue) {
    return window.localStorage[key] || defaultValue;
}


module.exports = {
    addToRecent,
    getRecent,
    set,
    get
};