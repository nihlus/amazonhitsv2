
let win;

function init(window) {

    win = window;

}


function clear() {

    win.cookies.getAll({ domain: '.amazon.com' }, cookies => {

        cookies.forEach(c => {

            win.cookies.remove({
                url: `https://www.amazon.com${c.path}`,
                name: c.name
            });

        }); // forEach cookie

    }); // getAll

}


function get(isShowAll, callback) {

    let acc = [];

    win.cookies.getAll({ domain: '.amazon.com' }, cookies => {
        
        cookies.forEach(c => {

            if (isShowAll) {

                acc.push(`${c.name}: ${c.value}`);
            }
            else if (['session-token', 'session-id', 'session-id-time'].includes(c.name)) {

                acc.push(`${c.name}: ${c.value}`);
            }

        }); // forEach cookie

        return callback(null, acc);

    }); // getAll cookies 
}


function stringify(domain, callback) {

    win.cookies.getAll({ domain }, cookies => {

        try {

            let acc = [];

            cookies.forEach(c => {

                const keys = Object.keys(c);

                let pair = [];
                keys.forEach(k => {

                    if (['name', 'value'].includes(k)) {
                        pair.push(`${k}=${c[k]}`);
                    }

                }); // forEach

                acc.push(pair.join(';'));

            }); // forEach

            // string fomat:
            // cookie1key1=cookie1value1;cookie1key2=cookie1value2;..,..,cookieNkey1=cookieNvalue1;cookieNkey2=cookieNvalue2;..

            return callback(null, acc.join(','));

        }
        catch (err) {
            return callback(err);
        }

    }); // getAll cookies

}


function inject(cookies) {

    if (cookies && cookies.length) {

        cookies.forEach(c => {
            //console.log(`set cookie ${c.name} -> ${c.value}`);
            win.cookies.set(_createAmazonCookie(c.name, c.value));
        });

    }

}


//-- implementation


function _createAmazonCookie(name, value) {
    
    return {
        url: "https://www.amazon.com/",
        name,
        value,
        domain: '.amazon.com',
        path: '/',
        secure: true,
        httpOnly: true,
        storeId: '0'
   
    };

}



module.exports = {
    init,
    clear,
    get,
    stringify,
    inject
}