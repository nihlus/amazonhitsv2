
const fs = require('fs');
const path = require('path');
const os = require('os');

let timestamp;
let config;
let logActivity;
let logUser;
let logError;
let logRequest;
let logProgram;


function init(cnfg) {

    config = cnfg;

    timestamp = Date.now();

    logActivity = _init('logActivity');
    logUser = _init('logUser');
    logError = _init('logError');
    logRequest = _init('logRequest');
    logProgram = _init('logProgram');
}


function activity(account, job, opts) {
    if (!logActivity || !account || !job) return;

    const sid = account.sessionId || '';
    const asin = opts.asin || job.asin;
    const isSearch = (opts.isSearch ? 1 : 0);
    const isClick = (opts.isClick ? 1 : 0);
    const pageClick = opts.pageClick || '';
    const isAdd = (opts.isAdd ? 1 : 0);
    const isPurchase = (opts.isPurchase ? 1 : 0);
    const pageFound = opts.pageFound || '';

    const str = `${Date.now()},${config.cloneId},${account.ip},${account.login},${sid},${asin},${job.terms},${isSearch},${isClick},${pageClick},${isAdd},${isPurchase},${pageFound}${os.EOL}`;
    fs.appendFileSync(logActivity.filepath, str);
}


function user(account, opts) {
    if (!logUser || !account) return;

    const sid = account.sessionId || '';
    const isLogin = (opts.isLogin ? 1 : 0);
    const isLogout = (opts.isLogout ? 1 : 0);
    const isSuccess = (opts.isSuccess ? 1 : 0);
    const isCaptcha = (opts.isCaptcha ? 1 : 0);

    const str = `${Date.now()},${config.cloneId},${account.ip},${account.login},${sid},${isLogin},${isLogout},${isSuccess},${isCaptcha}${os.EOL}`;
    fs.appendFileSync(logUser.filepath, str);
}


function error(account, opts) {
    if (!logError || !account) return;

    const sid = account.sessionId || '';
    const eventType = opts.eventType || '';

    const str = `${Date.now()},${config.cloneId},${account.ip},${account.login},${sid},${eventType}${os.EOL}`;
    fs.appendFileSync(logError.filepath, str);
}


function request(account, data) {
    if (!logRequest || !account) return;

    const str = `${Date.now()},${config.cloneId},${account.ip},${data}${os.EOL}`;
    fs.appendFileSync(logRequest.filepath, str);
}


function program(account, eventType, eventValue) {
    if (!logProgram) return;

    const ip = (account ? account.ip : '');

    const str = `${Date.now()},${config.cloneId},${ip},${eventType},${eventValue}${os.EOL}`;
    fs.appendFileSync(logProgram.filepath, str);
}


//-- implementation


function _init(logName) {

    let log = config[logName];
    if (!log.isEnabled) return;

    log.filepath = (path.isAbsolute(log.path) ? log.path : path.join(config.dir, log.path));
    log.filepath = path.join(log.filepath, `${logName}-${config.cloneId}-${timestamp}.log`);

    return log;
}


module.exports = {
    init,
    activity,
    user,
    error,
    request,
    program
};